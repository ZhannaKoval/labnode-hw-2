const {Router} = require('express');
const router = Router();
const controller = require('../controllers/notes');
const {authMiddleware} = require('./middlewares/authMiddle');
const textValidation = require('./middlewares/textValid');


router.use(authMiddleware);

router.get('/', controller.getAllNotes);
router.post('/', textValidation, controller.addANote);
router.get('/:id', controller.getCertainNote);
router.put('/:id', controller.changeCertainNote);
router.patch('/:id', controller.updateCertainNote);
router.delete('/:id', controller.deleteCertainNote);


module.exports = router;
