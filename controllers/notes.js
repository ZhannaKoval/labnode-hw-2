const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {Note} = require('../models/Note');


module.exports.getAllNotes = async function(req, res) {
  try {
    const {skip = 0, limit = 3} = req.query;


    const allNotes = await Note.find(
        {userID: req.user._id},
        [],
        {
          skip: parseInt(skip),
          limit: limit > 100 ? 3 : parseInt(limit),
        },
    );
    /*if (allNotes.length === 0) {
      return res.status(400).json({message: 'No note has been already added'});
    }*/

    return res.status(200).json({notes: allNotes});
  } catch (e) {
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

module.exports.addANote = async function(req, res) {
  try {
    const note = new Note({
      text: req.body.text,
    });
    note.userID = req.user._id;
    await note.save();
    return res.status(200).json({
      message: 'Success',
    });
  } catch (e) {
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

module.exports.getCertainNote = async function(req, res) {
  try {
    const {id} = req.params;

    const certainNote = await Note.findOne({_id: id, userID: req.user._id});
    if (!certainNote) {
      return res.status(400).json({message: 'Wrong id'});
    }
    return res.status(200).json({note: certainNote});
  } catch (e) {
    return res.status(400).json({
      message: 'Wrong id',
    });
  }
};

module.exports.changeCertainNote = async function(req, res) {
  try {
    const {id} = req.params;
    const {text} = req.body;
    const certainNote = await Note.findOne({_id: id, userID: req.user._id});
    if (!certainNote) {
      return res.status(400).json({message: 'Wrong id'});
    }
    certainNote.text = text;
    certainNote.save();
    return res.status(200).json({
      message: 'Success',
    });
  } catch (e) {
    return res.status(400).json({
      message: 'Wrong id',
    });
  }
};

module.exports.updateCertainNote = async function(req, res) {
  try {
    const {id} = req.params;
    const certainNote = await Note.findOne({_id: id, userID: req.user._id});
    if (!certainNote) {
      return res.status(400).json({message: 'Wrong id'});
    }

    if (!certainNote.completed) {
      certainNote.completed = true;
    } else {
      certainNote.completed = false;
    }
    certainNote.save();
    return res.status(200).json({
      message: 'Success',
    });
  } catch (e) {
    return res.status(400).json({
      message: 'Wrong id',
    });
  }
};

module.exports.deleteCertainNote = async function(req, res) {
  try {
    const {id} = req.params;
    const note = await Note.findOne({_id: id, userID: req.user._id});
    if (!note) {
      return res.status(400).json({message: 'Wrong id'});
    }
    note.deleteOne({_id: note._id});

    return res.status(200).json({message: 'Success'});
  } catch (e) {
    return res.status(400).json({
      message: 'Wrong id',
    });
  }
};
