const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/users');
const noteRoutes = require('./routes/notes');


const PORT = process.env.PORT || 8080;

app.use(morgan('dev'));
app.use(cors('dev'));
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use('/api/auth', authRoutes);
app.use('/api/users', userRoutes);
app.use('/api/notes', noteRoutes);


app.use((err, req, res, next) => {
  return res.status(400).json({message: err.message});
});

async function start() {
  try {
    await mongoose.connect('mongodb+srv://Zhanna_Koval:1408@cluster0.hmzc9.mongodb.net/DBforNotes?retryWrites=true&w=majority', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
    app.listen(PORT, ()=>{
      console.log(`Server has been started on port ${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
}

start();

app.all('*', function(req, res, next) {
  res.status(404).send('Not found');
});


