const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
  notes: [{
    ref: 'notes',
    type: Schema.Types.ObjectId,
  }],
});

module.exports.User = mongoose.model('users', userSchema);
