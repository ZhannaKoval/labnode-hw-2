const {Router} = require('express');
const router = Router();
const controller = require('../controllers/users');
const {authMiddleware} = require('./middlewares/authMiddle');
const newPassValidation = require('./middlewares/newPassValid');

router.use(authMiddleware);

router.get('/me', controller.UserProfileInfo);
router.delete('/me', controller.deleteUserProfile);
router.patch('/me', newPassValidation, controller.updateUserProfile);


module.exports = router;
