const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const {User} = require('../models/User');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {authMiddleware} = require('../routes/middlewares/authMiddle');
const bcrypt = require('bcrypt');


module.exports.UserProfileInfo = async function(req, res) {
  try {
    const user = await User.findOne({_id: req.user._id});

    return res.status(200).json({'user': {
      '_id': user._id,
      'username': user.username,
      'createdDate': user.createdDate,
    },
    });
  } catch (e) {
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

module.exports.deleteUserProfile = async function(req, res) {
  try {
    const user = await User.findOne({_id: req.user._id});
    user.deleteOne({_id: user._id});

    return res.status(200).json({message: 'Success'});
  } catch (e) {
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};

module.exports.updateUserProfile = async function(req, res) {
  try {
    const {oldPassword, newPassword} = req.body;
    const user = await User.findOne({_id: req.user._id});

    if ( !(await bcrypt.compare(oldPassword, user.password)) ) {
      return res.status(400).json({message: `Wrong old password!`});
    }

    if(oldPassword.split("").sort().join("") === newPassword.split("").sort().join("")) {
      return res.status(400).json({message: `Passwords are the same!`});
    }

    user.password = await bcrypt.hash(newPassword, 10);;
    await user.save();

    return res.status(200).json({message: 'Success'});
  } catch (e) {
    return res.status(500).json({
      message: 'Internal server error',
    });
  }
};
