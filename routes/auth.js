const express = require('express');
const router = express.Router();
const {login, register} = require('../controllers/auth');
const jwt = require('jsonwebtoken');
const {asyncWrapper} = require('./helper');
const {validRegistr} = require('./middlewares/validationMiddle');


router.post('/login', asyncWrapper(validRegistr), asyncWrapper(login));
router.post('/register', asyncWrapper(validRegistr), asyncWrapper(register));


module.exports = router;
